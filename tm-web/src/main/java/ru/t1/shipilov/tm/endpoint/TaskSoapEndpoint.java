package ru.t1.shipilov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.shipilov.tm.api.service.dto.ITaskDtoService;
import ru.t1.shipilov.tm.dto.soap.*;
import ru.t1.shipilov.tm.model.CustomUser;

@Endpoint
public class TaskSoapEndpoint {

    @NotNull
    public final static String LOCATION_URI = "/ws";

    @NotNull
    public final static String PORT_TYPE_NAME = "TaskSoapEndpointPort";

    @NotNull
    public final static String NAMESPACE = "http://tm.shipilov.t1.ru/dto/soap";


    @NotNull
    @Autowired
    private ITaskDtoService taskService;

    @Nullable
    @ResponsePayload
    @PayloadRoot(localPart = "taskFindByIdRequest", namespace = NAMESPACE)
    public TaskFindByIdResponse findOne(
            @AuthenticationPrincipal final CustomUser user,
            @RequestPayload final TaskFindByIdRequest request) {
        return new TaskFindByIdResponse(taskService.findOneById(user.getUserId(), request.getId()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskSaveRequest", namespace = NAMESPACE)
    public TaskSaveResponse saveOne(
            @AuthenticationPrincipal final CustomUser user,
            @RequestPayload final TaskSaveRequest request) {
        taskService.save(user.getUserId(), request.getTask());
        return new TaskSaveResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskUpdateRequest", namespace = NAMESPACE)
    public TaskUpdateResponse updateOne(
            @AuthenticationPrincipal final CustomUser user,
            @RequestPayload final TaskUpdateRequest request) {
        taskService.save(user.getUserId(), request.getTask());
        return new TaskUpdateResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteRequest", namespace = NAMESPACE)
    public TaskDeleteResponse deleteOne(
            @AuthenticationPrincipal final CustomUser user,
            @RequestPayload final TaskDeleteRequest request) {
        taskService.removeOneById(user.getUserId(), request.getId());
        return new TaskDeleteResponse();
    }

}
