package ru.t1.shipilov.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.shipilov.tm.api.service.dto.ITaskDtoService;
import ru.t1.shipilov.tm.dto.TaskDTO;
import ru.t1.shipilov.tm.exception.AuthException;
import ru.t1.shipilov.tm.exception.IdEmptyException;
import ru.t1.shipilov.tm.exception.NameEmptyException;
import ru.t1.shipilov.tm.repository.dto.ITaskDtoRepository;

import java.util.Collection;
import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class TaskDtoService implements ITaskDtoService {

    @NotNull
    @Autowired
    private ITaskDtoRepository repository;

    @Override
    @Transactional
    @SneakyThrows
    public void save(@Nullable final String userId, @Nullable final TaskDTO task) {
        if (userId == null) throw new AuthException();
        if (task == null) throw new IdEmptyException();
        if (task.getName() == null || task.getName().isEmpty()) throw new NameEmptyException();
        task.setUserId(userId);
        repository.save(task);
    }

    @Override
    @Transactional
    @SneakyThrows
    public void saveAll(@Nullable final String userId, @Nullable final Collection<TaskDTO> tasks) {
        if (userId == null) throw new AuthException();
        if (tasks == null) throw new IdEmptyException();
        if (tasks.isEmpty()) throw new IdEmptyException();
        for (@NotNull TaskDTO taskDTO : tasks) {
            taskDTO.setUserId(userId);
            if (taskDTO.getName() == null || taskDTO.getName().isEmpty()) throw new NameEmptyException();
        }
        repository.saveAll(tasks);
    }

    @Override
    @Transactional
    @SneakyThrows
    public void removeAll(@Nullable final String userId) {
        if (userId == null) throw new AuthException();
        repository.deleteAllByUserId(userId);
    }

    @Override
    @Transactional
    @SneakyThrows
    public void removeAll(@Nullable final String userId, @Nullable final Collection<TaskDTO> tasks) {
        if (userId == null) throw new AuthException();
        repository.deleteAll(tasks);
    }

    @Override
    @Transactional
    @SneakyThrows
    public void removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null) throw new AuthException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        repository.deleteByIdAndUserId(id, userId);
    }

    @Override
    @Transactional
    @SneakyThrows
    public void removeOne(@Nullable final String userId, @Nullable final TaskDTO task) {
        if (userId == null) throw new AuthException();
        if (task == null) throw new IdEmptyException();
        repository.deleteByIdAndUserId(task.getId(), userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskDTO> findAll(@Nullable final String userId) {
        if (userId == null) throw new AuthException();
        return repository.findAllByUserId(userId);
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null) throw new AuthException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findByIdAndUserId(id, userId);
    }

}
