package ru.t1.shipilov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.shipilov.tm.dto.Result;
import ru.t1.shipilov.tm.dto.soap.*;
import ru.t1.shipilov.tm.repository.dto.IUserDtoRepository;

import javax.annotation.Resource;

@Endpoint
public class AuthSoapEndpoint {

    @NotNull
    public final static String LOCATION_URI = "/ws";

    @NotNull
    public final static String PORT_TYPE_NAME = "AuthSoapEndpointPort";

    @NotNull
    public final static String NAMESPACE = "http://tm.shipilov.t1.ru/dto/soap";

    @Resource
    private AuthenticationManager authenticationManager;

    @Autowired
    private IUserDtoRepository userDtoRepository;

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "authLoginRequest", namespace = NAMESPACE)
    public AuthLoginResponse login(@RequestPayload final AuthLoginRequest request) {
        try {
            System.out.println(request.getUsername());
            System.out.println(request.getPassword());
            final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
                    request.getUsername(),
                    request.getPassword()
            );
            System.out.println(token);
            final Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return new AuthLoginResponse(new Result(authentication.isAuthenticated()));
        } catch (final Exception exception) {
            return new AuthLoginResponse(new Result(exception));
        }
    }

    @Nullable
    @ResponsePayload
    @PayloadRoot(localPart = "authProfileRequest", namespace = NAMESPACE)
    public AuthProfileResponse profile(@RequestPayload final AuthProfileRequest request) {
        final SecurityContext securityContext = SecurityContextHolder.getContext();
        final Authentication authentication = securityContext.getAuthentication();
        final String username = authentication.getName();
        return new AuthProfileResponse(userDtoRepository.findByLogin(username));
    }

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "authLogoutRequest", namespace = NAMESPACE)
    public AuthLogoutResponse logout(@RequestPayload final AuthLogoutRequest request) {
        SecurityContextHolder.getContext().setAuthentication(null);
        return new AuthLogoutResponse(new Result());
    }

}
