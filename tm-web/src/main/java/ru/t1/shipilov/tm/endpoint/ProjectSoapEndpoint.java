package ru.t1.shipilov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.shipilov.tm.api.service.dto.IProjectDtoService;
import ru.t1.shipilov.tm.api.service.model.IProjectService;
import ru.t1.shipilov.tm.dto.soap.*;
import ru.t1.shipilov.tm.model.CustomUser;

@Endpoint
public class ProjectSoapEndpoint {

    @NotNull
    public final static String LOCATION_URI = "/ws";

    @NotNull
    public final static String PORT_TYPE_NAME = "ProjectSoapEndpointPort";

    @NotNull
    public final static String NAMESPACE = "http://tm.shipilov.t1.ru/dto/soap";


    @NotNull
    @Autowired
    private IProjectDtoService projectDtoService;

    @NotNull
    @Autowired
    private IProjectService projectService;

    @Nullable
    @ResponsePayload
    @PayloadRoot(localPart = "projectFindByIdRequest", namespace = NAMESPACE)
    public ProjectFindByIdResponse findOne(
            @AuthenticationPrincipal final CustomUser user,
            @RequestPayload final ProjectFindByIdRequest request) {
        return new ProjectFindByIdResponse(projectDtoService.findOneById(user.getUserId(), request.getId()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectSaveRequest", namespace = NAMESPACE)
    public ProjectSaveResponse saveOne(
            @AuthenticationPrincipal final CustomUser user,
            @RequestPayload final ProjectSaveRequest request) {
        projectDtoService.save(user.getUserId(), request.getProject());
        return new ProjectSaveResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectUpdateRequest", namespace = NAMESPACE)
    public ProjectUpdateResponse updateOne(
            @AuthenticationPrincipal final CustomUser user,
            @RequestPayload final ProjectUpdateRequest request) {
        projectDtoService.save(user.getUserId(), request.getProject());
        return new ProjectUpdateResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteRequest", namespace = NAMESPACE)
    public ProjectDeleteResponse deleteOne(
            @AuthenticationPrincipal final CustomUser user,
            @RequestPayload final ProjectDeleteRequest request) {
        projectService.removeOneById(user.getUserId(), request.getId());
        return new ProjectDeleteResponse();
    }

}
