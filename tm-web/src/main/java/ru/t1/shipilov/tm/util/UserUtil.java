package ru.t1.shipilov.tm.util;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.t1.shipilov.tm.model.CustomUser;
import org.springframework.security.access.AccessDeniedException;

public final class UserUtil {

    private UserUtil() {
    }

    public static String getUserId() {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        final Object principal = authentication.getPrincipal();
        if (principal == null) throw new AccessDeniedException("");
        if (!(principal instanceof CustomUser)) throw new AccessDeniedException("");
        final CustomUser customUser = (CustomUser) principal;
        return customUser.getUserId();
    }

}
