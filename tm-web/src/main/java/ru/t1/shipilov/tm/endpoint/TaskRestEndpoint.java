package ru.t1.shipilov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import ru.t1.shipilov.tm.api.service.dto.ITaskDtoService;
import ru.t1.shipilov.tm.dto.TaskDTO;
import ru.t1.shipilov.tm.model.CustomUser;

@RestController
@RequestMapping("/api/task")
public class TaskRestEndpoint {

    @NotNull
    @Autowired
    private ITaskDtoService taskService;

    @Nullable
    @GetMapping("/{id}")
    public TaskDTO get(
            @AuthenticationPrincipal final CustomUser user,
            @NotNull @PathVariable("id") String id) {
        return taskService.findOneById(user.getUserId(), id);
    }

    @PostMapping
    public void post(
            @AuthenticationPrincipal final CustomUser user,
            @NotNull @RequestBody TaskDTO task) {
        taskService.save(user.getUserId(), task);
    }

    @PutMapping
    public void put(
            @AuthenticationPrincipal final CustomUser user,
            @NotNull @RequestBody TaskDTO task) {
        taskService.save(user.getUserId(), task);
    }

    @DeleteMapping("/{id}")
    public void delete(
            @AuthenticationPrincipal final CustomUser user,
            @NotNull @PathVariable("id") String id) {
        taskService.removeOneById(user.getUserId(), id);
    }

}
