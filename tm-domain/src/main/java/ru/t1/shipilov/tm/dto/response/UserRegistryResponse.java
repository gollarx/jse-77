package ru.t1.shipilov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.shipilov.tm.dto.model.UserDTO;

@Getter
@Setter
@NoArgsConstructor
public final class UserRegistryResponse extends AbstractUserResponse {

    public UserRegistryResponse(@Nullable final UserDTO user) {
        super(user);
    }

}
