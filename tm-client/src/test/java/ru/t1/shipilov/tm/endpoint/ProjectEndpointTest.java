package ru.t1.shipilov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.shipilov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.shipilov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.shipilov.tm.api.service.IPropertyService;
import ru.t1.shipilov.tm.dto.request.*;
import ru.t1.shipilov.tm.enumerated.Status;
import ru.t1.shipilov.tm.marker.SoapCategory;
import ru.t1.shipilov.tm.dto.model.ProjectDTO;
import ru.t1.shipilov.tm.service.PropertyService;

import java.util.UUID;

@Category(SoapCategory.class)
public class ProjectEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService.getHost(), propertyService.getPort());

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService.getHost(), propertyService.getPort());

    @Nullable
    private String firstToken;

    @Nullable
    private String secondToken;

    @Nullable
    private ProjectDTO project;

    @Before
    @SneakyThrows
    public void init() {
        firstToken = authEndpoint.login(new UserLoginRequest("user 1", "1")).getToken();
        secondToken = authEndpoint.login(new UserLoginRequest("user 2", "2")).getToken();
        @NotNull ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(firstToken);
        projectCreateRequest.setName("initial_proj_1");
        projectCreateRequest.setDescription("initial_desc_1");
        projectEndpoint.createProject(projectCreateRequest);
        projectCreateRequest = new ProjectCreateRequest(secondToken);
        projectCreateRequest.setName("initial_proj_2");
        projectCreateRequest.setDescription("initial_desc_2");
        projectEndpoint.createProject(projectCreateRequest);
    }

    @After
    @SneakyThrows
    public void end() {
        projectEndpoint.clearProject(new ProjectClearRequest(firstToken));
        projectEndpoint.clearProject(new ProjectClearRequest(secondToken));
        authEndpoint.logout(new UserLogoutRequest(firstToken));
        authEndpoint.logout(new UserLogoutRequest(secondToken));
    }

    @Test
    public void testProjectCreateNegative() {
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.createProject(new ProjectCreateRequest()));
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.createProject(new ProjectCreateRequest(firstToken)));
    }

    @Test
    @SneakyThrows
    public void testProjectCreatePositive() {
        @NotNull ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(firstToken);
        projectCreateRequest.setName("test_proj_1");
        projectCreateRequest.setDescription("test_desc_1");
        projectEndpoint.createProject(projectCreateRequest).getProject();
        Assert.assertEquals(2, projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects().size());
        projectCreateRequest = new ProjectCreateRequest(secondToken);
        projectCreateRequest.setName("test_proj_2");
        projectCreateRequest.setDescription("test_desc_2");
        projectEndpoint.createProject(projectCreateRequest).getProject();
        Assert.assertEquals(2, projectEndpoint.listProject(new ProjectListRequest(secondToken)).getProjects().size());
    }

    @Test
    public void testProjectListNegative() {
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.listProject(new ProjectListRequest()));
    }

    @Test
    public void testProjectListPositive() {
        Assert.assertEquals(1, projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects().size());
        Assert.assertEquals(
                "initial_proj_1",
                projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects().get(0).getName()
        );
        Assert.assertEquals(1, projectEndpoint.listProject(new ProjectListRequest(secondToken)).getProjects().size());
        Assert.assertEquals(
                "initial_proj_2",
                projectEndpoint.listProject(new ProjectListRequest(secondToken)).getProjects().get(0).getName()
        );
    }

    @Test
    public void testProjectRemoveByIdNegative() {
        ProjectRemoveByIdRequest projectRemoveByIdRequest = new ProjectRemoveByIdRequest();
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.removeProjectById(projectRemoveByIdRequest));
        projectRemoveByIdRequest.setToken(firstToken);
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.removeProjectById(projectRemoveByIdRequest));
        projectRemoveByIdRequest.setId("");
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.removeProjectById(new ProjectRemoveByIdRequest(firstToken)));
    }

    @Test
    @SneakyThrows
    public void testProjectRemoveByIdPositive() {
        Assert.assertNotNull(projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects());
        @NotNull String projectId = projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects().get(0).getId();
        Assert.assertEquals(1, projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects().size());
        @NotNull ProjectRemoveByIdRequest projectRemoveByIdRequest = new ProjectRemoveByIdRequest(firstToken);
        projectRemoveByIdRequest.setId(projectId);
        Assert.assertNotNull(projectEndpoint.removeProjectById(projectRemoveByIdRequest));
        Assert.assertNull(projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects());

        Assert.assertNotNull(projectEndpoint.listProject(new ProjectListRequest(secondToken)).getProjects());
        projectId = projectEndpoint.listProject(new ProjectListRequest(secondToken)).getProjects().get(0).getId();
        Assert.assertEquals(1, projectEndpoint.listProject(new ProjectListRequest(secondToken)).getProjects().size());
        projectRemoveByIdRequest = new ProjectRemoveByIdRequest(secondToken);
        projectRemoveByIdRequest.setId(projectId);
        Assert.assertNotNull(projectEndpoint.removeProjectById(projectRemoveByIdRequest));
        Assert.assertNull(projectEndpoint.listProject(new ProjectListRequest(secondToken)).getProjects());
    }

    @Test
    public void testProjectRemoveByIndexNegative() {
        ProjectRemoveByIndexRequest projectRemoveByIndexRequest = new ProjectRemoveByIndexRequest();
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.removeProjectByIndex(projectRemoveByIndexRequest));
        projectRemoveByIndexRequest.setToken(firstToken);
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.removeProjectByIndex(projectRemoveByIndexRequest));
        projectRemoveByIndexRequest.setIndex(9999);
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.removeProjectByIndex(projectRemoveByIndexRequest));
    }

    @Test
    @SneakyThrows
    public void testProjectRemoveByIndexPositive() {
        Assert.assertNotNull(projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects());
        Assert.assertEquals(1, projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects().size());
        @NotNull ProjectRemoveByIndexRequest projectRemoveByIndexRequest = new ProjectRemoveByIndexRequest(firstToken);
        projectRemoveByIndexRequest.setIndex(0);
        Assert.assertNotNull(projectEndpoint.removeProjectByIndex(projectRemoveByIndexRequest));
        Assert.assertNull(projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects());

        Assert.assertNotNull(projectEndpoint.listProject(new ProjectListRequest(secondToken)).getProjects());
        Assert.assertEquals(1, projectEndpoint.listProject(new ProjectListRequest(secondToken)).getProjects().size());
        projectRemoveByIndexRequest = new ProjectRemoveByIndexRequest(secondToken);
        projectRemoveByIndexRequest.setIndex(0);
        Assert.assertNotNull(projectEndpoint.removeProjectByIndex(projectRemoveByIndexRequest));
        Assert.assertNull(projectEndpoint.listProject(new ProjectListRequest(secondToken)).getProjects());
    }

    @Test
    public void testProjectCompleteByIdNegative() {
        ProjectCompleteByIdRequest projectCompleteByIdRequest = new ProjectCompleteByIdRequest();
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.completeProjectById(projectCompleteByIdRequest));
        projectCompleteByIdRequest.setToken(firstToken);
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.completeProjectById(projectCompleteByIdRequest));
        projectCompleteByIdRequest.setId("");
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.completeProjectById(projectCompleteByIdRequest));
        projectCompleteByIdRequest.setId(UUID.randomUUID().toString());
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.completeProjectById(projectCompleteByIdRequest));
    }

    @Test
    public void testProjectCompleteByIdPositive() {
        Assert.assertNotNull(projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects());
        Assert.assertEquals(Status.NOT_STARTED, projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects().get(0).getStatus());
        @NotNull String projectId = projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects().get(0).getId();
        @NotNull ProjectCompleteByIdRequest projectCompleteByIdRequest = new ProjectCompleteByIdRequest(firstToken);
        projectCompleteByIdRequest.setId(projectId);
        Assert.assertNotNull(projectEndpoint.completeProjectById(projectCompleteByIdRequest));
        Assert.assertEquals(Status.COMPLETED, projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects().get(0).getStatus());

        Assert.assertNotNull(projectEndpoint.listProject(new ProjectListRequest(secondToken)).getProjects());
        Assert.assertEquals(Status.NOT_STARTED, projectEndpoint.listProject(new ProjectListRequest(secondToken)).getProjects().get(0).getStatus());
        projectId = projectEndpoint.listProject(new ProjectListRequest(secondToken)).getProjects().get(0).getId();
        projectCompleteByIdRequest = new ProjectCompleteByIdRequest(secondToken);
        projectCompleteByIdRequest.setId(projectId);
        Assert.assertNotNull(projectEndpoint.completeProjectById(projectCompleteByIdRequest));
        Assert.assertEquals(Status.COMPLETED, projectEndpoint.listProject(new ProjectListRequest(secondToken)).getProjects().get(0).getStatus());
    }

    @Test
    public void testProjectCompleteByIndexNegative() {
        ProjectCompleteByIndexRequest projectCompleteByIndexRequest = new ProjectCompleteByIndexRequest();
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.completeProjectByIndex(projectCompleteByIndexRequest));
        projectCompleteByIndexRequest.setToken(firstToken);
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.completeProjectByIndex(projectCompleteByIndexRequest));
        projectCompleteByIndexRequest.setIndex(-1);
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.completeProjectByIndex(projectCompleteByIndexRequest));
        projectCompleteByIndexRequest.setIndex(9999);
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.completeProjectByIndex(projectCompleteByIndexRequest));
    }

    @Test
    @SneakyThrows
    public void testProjectCompleteByIndexPositive() {
        Assert.assertNotNull(projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects());
        Assert.assertEquals(Status.NOT_STARTED, projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects().get(0).getStatus());
        @NotNull ProjectCompleteByIndexRequest projectCompleteByIndexRequest = new ProjectCompleteByIndexRequest(firstToken);
        projectCompleteByIndexRequest.setIndex(0);
        @Nullable ProjectDTO completedProject = projectEndpoint.completeProjectByIndex(projectCompleteByIndexRequest).getProject();
        Assert.assertNotNull(completedProject);
        @NotNull ProjectShowByIdRequest projectShowByIdRequest = new ProjectShowByIdRequest(firstToken);
        projectShowByIdRequest.setId(completedProject.getId());
        Assert.assertEquals(Status.COMPLETED, projectEndpoint.showProjectById(projectShowByIdRequest).getProject().getStatus());

        Assert.assertNotNull(projectEndpoint.listProject(new ProjectListRequest(secondToken)).getProjects());
        Assert.assertEquals(Status.NOT_STARTED, projectEndpoint.listProject(new ProjectListRequest(secondToken)).getProjects().get(0).getStatus());
        projectCompleteByIndexRequest = new ProjectCompleteByIndexRequest(secondToken);
        projectCompleteByIndexRequest.setIndex(0);
        completedProject = projectEndpoint.completeProjectByIndex(projectCompleteByIndexRequest).getProject();
        Assert.assertNotNull(completedProject);
        projectShowByIdRequest = new ProjectShowByIdRequest(secondToken);
        projectShowByIdRequest.setId(completedProject.getId());
        Assert.assertEquals(Status.COMPLETED, projectEndpoint.showProjectById(projectShowByIdRequest).getProject().getStatus());
    }

    @Test
    public void testProjectStartByIdNegative() {
        ProjectStartByIdRequest projectStartByIdRequest = new ProjectStartByIdRequest();
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.startProjectById(projectStartByIdRequest));
        projectStartByIdRequest.setToken(firstToken);
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.startProjectById(projectStartByIdRequest));
        projectStartByIdRequest.setId("");
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.startProjectById(projectStartByIdRequest));
        projectStartByIdRequest.setId(UUID.randomUUID().toString());
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.startProjectById(projectStartByIdRequest));
    }

    @Test
    public void testProjectStartByIdPositive() {
        Assert.assertNotNull(projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects());
        Assert.assertEquals(Status.NOT_STARTED, projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects().get(0).getStatus());
        @NotNull String projectId = projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects().get(0).getId();
        @NotNull ProjectStartByIdRequest projectStartByIdRequest = new ProjectStartByIdRequest(firstToken);
        projectStartByIdRequest.setId(projectId);
        Assert.assertNotNull(projectEndpoint.startProjectById(projectStartByIdRequest));
        Assert.assertEquals(Status.IN_PROGRESS, projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects().get(0).getStatus());

        Assert.assertNotNull(projectEndpoint.listProject(new ProjectListRequest(secondToken)).getProjects());
        Assert.assertEquals(Status.NOT_STARTED, projectEndpoint.listProject(new ProjectListRequest(secondToken)).getProjects().get(0).getStatus());
        projectId = projectEndpoint.listProject(new ProjectListRequest(secondToken)).getProjects().get(0).getId();
        projectStartByIdRequest = new ProjectStartByIdRequest(secondToken);
        projectStartByIdRequest.setId(projectId);
        Assert.assertNotNull(projectEndpoint.startProjectById(projectStartByIdRequest));
        Assert.assertEquals(Status.IN_PROGRESS, projectEndpoint.listProject(new ProjectListRequest(secondToken)).getProjects().get(0).getStatus());
    }

    @Test
    public void testProjectStartByIndexNegative() {
        ProjectStartByIndexRequest projectStartByIndexRequest = new ProjectStartByIndexRequest();
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.startProjectByIndex(projectStartByIndexRequest));
        projectStartByIndexRequest.setToken(firstToken);
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.startProjectByIndex(projectStartByIndexRequest));
        projectStartByIndexRequest.setIndex(-1);
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.startProjectByIndex(projectStartByIndexRequest));
        projectStartByIndexRequest.setIndex(9999);
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.startProjectByIndex(projectStartByIndexRequest));
    }

    @Test
    public void testProjectStartByIndexPositive() {
        Assert.assertNotNull(projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects());
        Assert.assertEquals(Status.NOT_STARTED, projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects().get(0).getStatus());
        @NotNull ProjectStartByIndexRequest projectStartByIndexRequest = new ProjectStartByIndexRequest(firstToken);
        projectStartByIndexRequest.setIndex(0);
        Assert.assertNotNull(projectEndpoint.startProjectByIndex(projectStartByIndexRequest));
        Assert.assertEquals(Status.IN_PROGRESS, projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects().get(0).getStatus());

        Assert.assertNotNull(projectEndpoint.listProject(new ProjectListRequest(secondToken)).getProjects());
        Assert.assertEquals(Status.NOT_STARTED, projectEndpoint.listProject(new ProjectListRequest(secondToken)).getProjects().get(0).getStatus());
        projectStartByIndexRequest = new ProjectStartByIndexRequest(secondToken);
        projectStartByIndexRequest.setIndex(0);
        Assert.assertNotNull(projectEndpoint.startProjectByIndex(projectStartByIndexRequest));
        Assert.assertEquals(Status.IN_PROGRESS, projectEndpoint.listProject(new ProjectListRequest(secondToken)).getProjects().get(0).getStatus());
    }

    @Test
    public void testProjectChangeStatusByIdNegative() {
        ProjectChangeStatusByIdRequest projectChangeStatusByIdRequest = new ProjectChangeStatusByIdRequest();
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.changeProjectStatusById(projectChangeStatusByIdRequest));
        projectChangeStatusByIdRequest.setToken(firstToken);
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.changeProjectStatusById(projectChangeStatusByIdRequest));
        projectChangeStatusByIdRequest.setId("");
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.changeProjectStatusById(projectChangeStatusByIdRequest));
        projectChangeStatusByIdRequest.setId(UUID.randomUUID().toString());
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.changeProjectStatusById(projectChangeStatusByIdRequest));
        projectChangeStatusByIdRequest.setId(projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects().get(0).getId());
        projectChangeStatusByIdRequest.setStatus(null);
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.changeProjectStatusById(projectChangeStatusByIdRequest));
    }

    @Test
    @SneakyThrows
    public void testProjectChangeStatusByIdPositive() {
        Assert.assertNotNull(projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects());
        Assert.assertEquals(Status.NOT_STARTED, projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects().get(0).getStatus());
        @NotNull String projectId = projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects().get(0).getId();
        @NotNull ProjectChangeStatusByIdRequest projectChangeStatusByIdRequest = new ProjectChangeStatusByIdRequest(firstToken);
        projectChangeStatusByIdRequest.setId(projectId);
        projectChangeStatusByIdRequest.setStatus(Status.IN_PROGRESS);
        Assert.assertNotNull(projectEndpoint.changeProjectStatusById(projectChangeStatusByIdRequest));
        Assert.assertEquals(Status.IN_PROGRESS, projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects().get(0).getStatus());

        Assert.assertNotNull(projectEndpoint.listProject(new ProjectListRequest(secondToken)).getProjects());
        Assert.assertEquals(Status.NOT_STARTED, projectEndpoint.listProject(new ProjectListRequest(secondToken)).getProjects().get(0).getStatus());
        projectId = projectEndpoint.listProject(new ProjectListRequest(secondToken)).getProjects().get(0).getId();
        projectChangeStatusByIdRequest = new ProjectChangeStatusByIdRequest(secondToken);
        projectChangeStatusByIdRequest.setId(projectId);
        projectChangeStatusByIdRequest.setStatus(Status.IN_PROGRESS);
        Assert.assertNotNull(projectEndpoint.changeProjectStatusById(projectChangeStatusByIdRequest));
        Assert.assertEquals(Status.IN_PROGRESS, projectEndpoint.listProject(new ProjectListRequest(secondToken)).getProjects().get(0).getStatus());
    }

    @Test
    public void testProjectChangeStatusByIndexNegative() {
        ProjectChangeStatusByIndexRequest projectChangeStatusByIndexRequest = new ProjectChangeStatusByIndexRequest();
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.changeProjectStatusByIndex(projectChangeStatusByIndexRequest));
        projectChangeStatusByIndexRequest.setToken(firstToken);
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.changeProjectStatusByIndex(projectChangeStatusByIndexRequest));
        projectChangeStatusByIndexRequest.setIndex(-1);
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.changeProjectStatusByIndex(projectChangeStatusByIndexRequest));
        projectChangeStatusByIndexRequest.setIndex(9999);
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.changeProjectStatusByIndex(projectChangeStatusByIndexRequest));
        projectChangeStatusByIndexRequest.setIndex(0);
        projectChangeStatusByIndexRequest.setStatus(null);
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.changeProjectStatusByIndex(projectChangeStatusByIndexRequest));
    }

    @Test
    @SneakyThrows
    public void testProjectChangeStatusByIndexPositive() {
        Assert.assertNotNull(projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects());
        Assert.assertEquals(Status.NOT_STARTED, projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects().get(0).getStatus());
        @NotNull ProjectChangeStatusByIndexRequest projectChangeStatusByIndexRequest = new ProjectChangeStatusByIndexRequest(firstToken);
        projectChangeStatusByIndexRequest.setIndex(0);
        projectChangeStatusByIndexRequest.setStatus(Status.IN_PROGRESS);
        Assert.assertNotNull(projectEndpoint.changeProjectStatusByIndex(projectChangeStatusByIndexRequest));
        Assert.assertEquals(Status.IN_PROGRESS, projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects().get(0).getStatus());

        Assert.assertNotNull(projectEndpoint.listProject(new ProjectListRequest(secondToken)).getProjects());
        Assert.assertEquals(Status.NOT_STARTED, projectEndpoint.listProject(new ProjectListRequest(secondToken)).getProjects().get(0).getStatus());
        projectChangeStatusByIndexRequest = new ProjectChangeStatusByIndexRequest(secondToken);
        projectChangeStatusByIndexRequest.setIndex(0);
        projectChangeStatusByIndexRequest.setStatus(Status.IN_PROGRESS);
        Assert.assertNotNull(projectEndpoint.changeProjectStatusByIndex(projectChangeStatusByIndexRequest));
        Assert.assertEquals(Status.IN_PROGRESS, projectEndpoint.listProject(new ProjectListRequest(secondToken)).getProjects().get(0).getStatus());
    }

    @Test
    public void testProjectShowByIdNegative() {
        ProjectShowByIdRequest projectChangeStatusByIdRequest = new ProjectShowByIdRequest();
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.showProjectById(projectChangeStatusByIdRequest));
        projectChangeStatusByIdRequest.setToken(firstToken);
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.showProjectById(projectChangeStatusByIdRequest));
        projectChangeStatusByIdRequest.setId("");
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.showProjectById(projectChangeStatusByIdRequest));
    }

    @Test
    @SneakyThrows
    public void testProjectShowByIdPositive() {
        Assert.assertNotNull(projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects());
        @NotNull String projectId = projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects().get(0).getId();
        @NotNull ProjectShowByIdRequest projectShowByIdRequest = new ProjectShowByIdRequest(firstToken);
        projectShowByIdRequest.setId(projectId);
        Assert.assertNotNull(projectEndpoint.showProjectById(projectShowByIdRequest));
        Assert.assertEquals("initial_proj_1", projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects().get(0).getName());

        Assert.assertNotNull(projectEndpoint.listProject(new ProjectListRequest(secondToken)).getProjects());
        projectId = projectEndpoint.listProject(new ProjectListRequest(secondToken)).getProjects().get(0).getId();
        projectShowByIdRequest = new ProjectShowByIdRequest(secondToken);
        projectShowByIdRequest.setId(projectId);
        Assert.assertNotNull(projectEndpoint.showProjectById(projectShowByIdRequest));
        Assert.assertEquals("initial_proj_2", projectEndpoint.listProject(new ProjectListRequest(secondToken)).getProjects().get(0).getName());
    }

    @Test
    public void testProjectShowByIndexNegative() {
        ProjectShowByIndexRequest projectChangeStatusByIndexRequest = new ProjectShowByIndexRequest();
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.showProjectByIndex(projectChangeStatusByIndexRequest));
        projectChangeStatusByIndexRequest.setToken(firstToken);
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.showProjectByIndex(projectChangeStatusByIndexRequest));
        projectChangeStatusByIndexRequest.setIndex(-1);
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.showProjectByIndex(projectChangeStatusByIndexRequest));
    }

    @Test
    @SneakyThrows
    public void testProjectShowByIndexPositive() {
        Assert.assertNotNull(projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects());
        @NotNull ProjectShowByIndexRequest projectShowByIndexRequest = new ProjectShowByIndexRequest(firstToken);
        projectShowByIndexRequest.setIndex(0);
        Assert.assertNotNull(projectEndpoint.showProjectByIndex(projectShowByIndexRequest));
        Assert.assertEquals("initial_proj_1", projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects().get(0).getName());

        Assert.assertNotNull(projectEndpoint.listProject(new ProjectListRequest(secondToken)).getProjects());
        projectShowByIndexRequest = new ProjectShowByIndexRequest(secondToken);
        projectShowByIndexRequest.setIndex(0);
        Assert.assertNotNull(projectEndpoint.showProjectByIndex(projectShowByIndexRequest));
        Assert.assertEquals("initial_proj_2", projectEndpoint.listProject(new ProjectListRequest(secondToken)).getProjects().get(0).getName());
    }

    @Test
    public void testProjectUpdateByIdNegative() {
        ProjectUpdateByIdRequest projectUpdateByIdRequest = new ProjectUpdateByIdRequest();
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.updateProjectById(projectUpdateByIdRequest));
        projectUpdateByIdRequest.setToken(firstToken);
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.updateProjectById(projectUpdateByIdRequest));
        projectUpdateByIdRequest.setId("");
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.updateProjectById(projectUpdateByIdRequest));
        projectUpdateByIdRequest.setId(UUID.randomUUID().toString());
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.updateProjectById(projectUpdateByIdRequest));
        projectUpdateByIdRequest.setName("updated_init_proj_1");
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.updateProjectById(projectUpdateByIdRequest));
    }

    @Test
    @SneakyThrows
    public void testProjectUpdateByIdPositive() {
        Assert.assertNotNull(projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects());
        @NotNull String projectId = projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects().get(0).getId();
        @NotNull ProjectUpdateByIdRequest projectUpdateByIdRequest = new ProjectUpdateByIdRequest(firstToken);
        projectUpdateByIdRequest.setId(projectId);
        projectUpdateByIdRequest.setName("updated_init_proj_1");
        projectUpdateByIdRequest.setDescription("updated_desc_proj_1");
        Assert.assertNotNull(projectEndpoint.updateProjectById(projectUpdateByIdRequest));
        Assert.assertEquals("updated_init_proj_1", projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects().get(0).getName());
        Assert.assertEquals("updated_desc_proj_1", projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects().get(0).getDescription());

        Assert.assertNotNull(projectEndpoint.listProject(new ProjectListRequest(secondToken)).getProjects());
        projectId = projectEndpoint.listProject(new ProjectListRequest(secondToken)).getProjects().get(0).getId();
        projectUpdateByIdRequest = new ProjectUpdateByIdRequest(secondToken);
        projectUpdateByIdRequest.setId(projectId);
        projectUpdateByIdRequest.setName("updated_init_proj_2");
        projectUpdateByIdRequest.setDescription("updated_desc_proj_2");
        Assert.assertNotNull(projectEndpoint.updateProjectById(projectUpdateByIdRequest));
        Assert.assertEquals("updated_init_proj_2", projectEndpoint.listProject(new ProjectListRequest(secondToken)).getProjects().get(0).getName());
        Assert.assertEquals("updated_desc_proj_2", projectEndpoint.listProject(new ProjectListRequest(secondToken)).getProjects().get(0).getDescription());
    }

    @Test
    public void testProjectUpdateByIndexNegative() {
        ProjectUpdateByIndexRequest projectUpdateByIndexRequest = new ProjectUpdateByIndexRequest();
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.updateProjectByIndex(projectUpdateByIndexRequest));
        projectUpdateByIndexRequest.setToken(firstToken);
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.updateProjectByIndex(projectUpdateByIndexRequest));
        projectUpdateByIndexRequest.setIndex(-1);
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.updateProjectByIndex(projectUpdateByIndexRequest));
        projectUpdateByIndexRequest.setIndex(9999);
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.updateProjectByIndex(projectUpdateByIndexRequest));
        projectUpdateByIndexRequest.setName("updated_init_proj_1");
        Assert.assertThrows(RuntimeException.class, () -> projectEndpoint.updateProjectByIndex(projectUpdateByIndexRequest));
    }

    @Test
    @SneakyThrows
    public void testProjectUpdateByIndexPositive() {
        Assert.assertNotNull(projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects());
        @NotNull ProjectUpdateByIndexRequest projectUpdateByIndexRequest = new ProjectUpdateByIndexRequest(firstToken);
        projectUpdateByIndexRequest.setIndex(0);
        projectUpdateByIndexRequest.setName("updated_init_proj_1");
        projectUpdateByIndexRequest.setDescription("updated_desc_proj_1");
        Assert.assertNotNull(projectEndpoint.updateProjectByIndex(projectUpdateByIndexRequest));
        Assert.assertEquals("updated_init_proj_1", projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects().get(0).getName());
        Assert.assertEquals("updated_desc_proj_1", projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects().get(0).getDescription());

        Assert.assertNotNull(projectEndpoint.listProject(new ProjectListRequest(secondToken)).getProjects());
        projectUpdateByIndexRequest = new ProjectUpdateByIndexRequest(secondToken);
        projectUpdateByIndexRequest.setIndex(0);
        projectUpdateByIndexRequest.setName("updated_init_proj_2");
        projectUpdateByIndexRequest.setDescription("updated_desc_proj_2");
        Assert.assertNotNull(projectEndpoint.updateProjectByIndex(projectUpdateByIndexRequest));
        Assert.assertEquals("updated_init_proj_2", projectEndpoint.listProject(new ProjectListRequest(secondToken)).getProjects().get(0).getName());
        Assert.assertEquals("updated_desc_proj_2", projectEndpoint.listProject(new ProjectListRequest(secondToken)).getProjects().get(0).getDescription());
    }

    @Test
    @SneakyThrows
    public void testProjectClear() {
        Assert.assertNotNull(projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects());
        @NotNull ProjectClearRequest projectClearRequest = new ProjectClearRequest(firstToken);
        Assert.assertNotNull(projectEndpoint.clearProject(projectClearRequest));
        Assert.assertNull(projectEndpoint.listProject(new ProjectListRequest(firstToken)).getProjects());

        Assert.assertNotNull(projectEndpoint.listProject(new ProjectListRequest(secondToken)).getProjects());
        projectClearRequest = new ProjectClearRequest(secondToken);
        Assert.assertNotNull(projectEndpoint.clearProject(projectClearRequest));
        Assert.assertNull(projectEndpoint.listProject(new ProjectListRequest(secondToken)).getProjects());
    }

}
