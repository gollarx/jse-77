package ru.t1.shipilov.tm.listener.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.shipilov.tm.dto.request.ProjectShowByIdRequest;
import ru.t1.shipilov.tm.dto.model.ProjectDTO;
import ru.t1.shipilov.tm.event.ConsoleEvent;
import ru.t1.shipilov.tm.util.TerminalUtil;

@Component
public final class ProjectShowByIdListener extends AbstractProjectListener {

    @NotNull
    private final String NAME = "project-show-by-id";

    @NotNull
    private final String DESCRIPTION = "Show project by Id.";

    @Override
    @SneakyThrows
    @EventListener(condition = "@projectShowByIdListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectShowByIdRequest request = new ProjectShowByIdRequest(getToken());
        request.setId(id);
        @Nullable final ProjectDTO project = projectEndpoint.showProjectById(request).getProject();
        showProject(project);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
