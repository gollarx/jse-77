package ru.t1.shipilov.tm.service;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.t1.shipilov.tm.api.service.ITokenService;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Service
public final class TokenService implements ITokenService {

    @NotNull
    private String token;

}
