package ru.t1.shipilov.tm.listener.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.shipilov.tm.dto.request.ProjectChangeStatusByIndexRequest;
import ru.t1.shipilov.tm.enumerated.Status;
import ru.t1.shipilov.tm.event.ConsoleEvent;
import ru.t1.shipilov.tm.util.TerminalUtil;

import java.util.Arrays;

@Component
public final class ProjectChangeStatusByIndexListener extends AbstractProjectListener {

    @NotNull
    private final String NAME = "project-change-status-by-index";

    @NotNull
    private final String DESCRIPTION = "Change project status by index.";

    @Override
    @SneakyThrows
    @EventListener(condition = "@projectChangeStatusByIndexListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[CHANGE PROJECT STATUS BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @NotNull final Status status = Status.toStatus(statusValue);
        @NotNull final ProjectChangeStatusByIndexRequest request = new ProjectChangeStatusByIndexRequest(getToken());
        request.setIndex(index);
        request.setStatus(status);
        projectEndpoint.changeProjectStatusByIndex(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
