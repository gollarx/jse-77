package ru.t1.shipilov.tm.service.dto;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.shipilov.tm.api.service.IPropertyService;
import ru.t1.shipilov.tm.api.service.dto.IUserDtoService;
import ru.t1.shipilov.tm.configuration.ServerConfiguration;
import ru.t1.shipilov.tm.dto.model.UserDTO;
import ru.t1.shipilov.tm.enumerated.Role;
import ru.t1.shipilov.tm.exception.entity.EntityNotFoundException;
import ru.t1.shipilov.tm.exception.entity.UserNotFoundException;
import ru.t1.shipilov.tm.exception.entity.field.EmailEmptyException;
import ru.t1.shipilov.tm.exception.entity.field.IdEmptyException;
import ru.t1.shipilov.tm.exception.entity.field.LoginEmptyException;
import ru.t1.shipilov.tm.exception.entity.field.PasswordEmptyException;
import ru.t1.shipilov.tm.exception.user.ExistsEmailException;
import ru.t1.shipilov.tm.exception.user.ExistsLoginException;
import ru.t1.shipilov.tm.exception.user.RoleEmptyException;
import ru.t1.shipilov.tm.marker.UnitCategory;
import ru.t1.shipilov.tm.migration.AbstractSchemeTest;
import ru.t1.shipilov.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static ru.t1.shipilov.tm.constant.UserConstant.*;

@Category(UnitCategory.class)
public class UserDtoServiceTest extends AbstractSchemeTest {

    @NotNull
    private static ApplicationContext CONTEXT = new AnnotationConfigApplicationContext(ServerConfiguration.class);

    @NotNull
    private static IPropertyService PROPERTY_SERVICE = CONTEXT.getBean(IPropertyService.class);

    @NotNull
    private static IUserDtoService USER_SERVICE = CONTEXT.getBean(IUserDtoService.class);

    @NotNull
    private List<UserDTO> userList;

    @BeforeClass
    public static void changeSchema() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    @Before
    public void init() {
        USER_SERVICE.clear();
        userList = new ArrayList<>();
        for (int i = 1; i <= INIT_COUNT_USERS; i++) {
            @NotNull final UserDTO user = new UserDTO();
            user.setLogin("User_" + i);
            user.setEmail("User_" + i + "@test.ru");
            user.setFirstName("User_" + i);
            user.setLastName("Petrov_" + i);
            user.setMiddleName("Petrovich_" + i);
            USER_SERVICE.add(user);
            userList.add(user);
        }
    }

    @After
    public void closeConnection() {
        USER_SERVICE.clear();
    }

    @AfterClass
    public static void defaultUsersRestore() {
        try {
            IUserDtoService userService = CONTEXT.getBean(IUserDtoService.class);
            userService.create("admin", "admin", Role.ADMIN);
            userService.create("user 1", "1", "first@site.com");
            userService.create("user 2", "2", "second@site.com");
        } catch (Exception ignored) {
        }
    }

    @Test
    public void testClearPositive() {
        Assert.assertEquals(INIT_COUNT_USERS, USER_SERVICE.findAll().size());
        USER_SERVICE.clear();
        Assert.assertEquals(0, USER_SERVICE.findAll().size());
    }

    @Test
    public void testFindAll() {
        @NotNull List<UserDTO> users = USER_SERVICE.findAll();
        Assert.assertNotNull(users);
        Assert.assertEquals(userList.size(), users.size());
        for (final UserDTO user : users) {
            Assert.assertNotNull(
                    userList.stream()
                            .filter(m -> user.getId().equals(m.getId()))
                            .findFirst()
                            .orElse(null)
            );
        }
    }

    @Test(expected = EntityNotFoundException.class)
    public void testAddUserNegative() {
        USER_SERVICE.add(NULLABLE_USER);
    }

    @Test
    public void testAddUser() {
        Assert.assertThrows(EntityNotFoundException.class, () -> USER_SERVICE.add(NULLABLE_USER));
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin("tstUsr");
        USER_SERVICE.add(user);
        Assert.assertEquals(INIT_COUNT_USERS + 1, USER_SERVICE.findAll().size());
    }

    @Test
    public void testAdd() {
        @NotNull final UserDTO user = new UserDTO();
        Assert.assertEquals(userList.size(), USER_SERVICE.findAll().size());
        USER_SERVICE.clear();
        userList.clear();
        user.setLogin("tstUsr");
        USER_SERVICE.add(user);
        userList.add(0, user);
        Assert.assertEquals(userList.size(), USER_SERVICE.findAll().size());
        for (int i = 0; i < userList.size(); i++)
            Assert.assertEquals(userList.get(i).getId(), USER_SERVICE.findAll().get(i).getId());
    }

    @Test
    public void testFindOneByIdNegative() {
        Assert.assertThrows(IdEmptyException.class, () -> USER_SERVICE.findById(NULLABLE_USER_ID));
        Assert.assertThrows(IdEmptyException.class, () -> USER_SERVICE.findById(EMPTY_USER_ID));
    }

    @Test
    public void testFindOneByIdPositive() {
        Assert.assertNull(USER_SERVICE.findById(UUID.randomUUID().toString()));
        for (final UserDTO user : userList) {
            Assert.assertEquals(user.getId(), USER_SERVICE.findById(user.getId()).getId());
        }
    }

    @Test(expected = UserNotFoundException.class)
    public void testRemoveNegative() {
        USER_SERVICE.remove(NULLABLE_USER);
    }

    @Test
    public void testRemoveByIdNegative() {
        Assert.assertThrows(IdEmptyException.class, () -> USER_SERVICE.removeById(NULLABLE_USER_ID));
        Assert.assertThrows(IdEmptyException.class, () -> USER_SERVICE.removeById(EMPTY_USER_ID));
    }

    @Test
    public void testRemoveByIdPositive() {
        for (final UserDTO user : userList) {
            USER_SERVICE.removeById(user.getId());
            Assert.assertFalse(USER_SERVICE.findAll().contains(user));
        }
        Assert.assertEquals(0, USER_SERVICE.findAll().size());
    }

    @Test
    public void testCreateNegative() {
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.create(NULLABLE_LOGIN, NULLABLE_PASSWORD));
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.create(EMPTY_LOGIN, NULLABLE_PASSWORD));
        Assert.assertThrows(ExistsLoginException.class, () -> USER_SERVICE.create(userList.get(0).getLogin(), NULLABLE_PASSWORD));
        Assert.assertThrows(PasswordEmptyException.class, () -> USER_SERVICE.create("USR", NULLABLE_PASSWORD));
        Assert.assertThrows(PasswordEmptyException.class, () -> USER_SERVICE.create("USR", EMPTY_PASSWORD));

        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.create(NULLABLE_LOGIN, NULLABLE_PASSWORD, NULLABLE_EMAIL));
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.create(EMPTY_LOGIN, NULLABLE_PASSWORD, NULLABLE_EMAIL));
        Assert.assertThrows(ExistsLoginException.class, () -> USER_SERVICE.create(userList.get(0).getLogin(), NULLABLE_PASSWORD, NULLABLE_EMAIL));
        Assert.assertThrows(PasswordEmptyException.class, () -> USER_SERVICE.create("USR", NULLABLE_PASSWORD, NULLABLE_EMAIL));
        Assert.assertThrows(PasswordEmptyException.class, () -> USER_SERVICE.create("USR", EMPTY_PASSWORD, NULLABLE_EMAIL));
        Assert.assertThrows(ExistsEmailException.class, () -> USER_SERVICE.create("USR", "USR", userList.get(0).getEmail()));

        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.create(NULLABLE_LOGIN, NULLABLE_PASSWORD, NULLABLE_ROLE));
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.create(EMPTY_LOGIN, NULLABLE_PASSWORD, NULLABLE_ROLE));
        Assert.assertThrows(ExistsLoginException.class, () -> USER_SERVICE.create(userList.get(0).getLogin(), NULLABLE_PASSWORD, NULLABLE_ROLE));
        Assert.assertThrows(PasswordEmptyException.class, () -> USER_SERVICE.create("USR", NULLABLE_PASSWORD, NULLABLE_ROLE));
        Assert.assertThrows(PasswordEmptyException.class, () -> USER_SERVICE.create("USR", EMPTY_PASSWORD, NULLABLE_ROLE));
        Assert.assertThrows(RoleEmptyException.class, () -> USER_SERVICE.create("USR", "USR", NULLABLE_ROLE));
    }

    @Test
    public void testCreatePositive() {
        Assert.assertNotNull(USER_SERVICE.create("USR", "USR", NULLABLE_EMAIL));
        Assert.assertNotNull(USER_SERVICE.create("USR2", "USR2", "EMAIL"));
        Assert.assertNotNull(USER_SERVICE.create("USR3", "USR3", Role.USUAL));
        Assert.assertEquals(INIT_COUNT_USERS + 3, USER_SERVICE.findAll().size());
    }

    @Test
    public void testUpdateUserNegative() {
        Assert.assertThrows(IdEmptyException.class, () -> USER_SERVICE.updateUser(NULLABLE_USER_ID, null, null, null));
        Assert.assertThrows(IdEmptyException.class, () -> USER_SERVICE.updateUser(EMPTY_USER_ID, null, null, null));
        Assert.assertThrows(UserNotFoundException.class, () -> USER_SERVICE.updateUser(UUID.randomUUID().toString(), null, null, null));
    }

    @Test
    public void testUpdateUserPositive() {
        for (final UserDTO user : userList) {
            Assert.assertNotEquals("FST", user.getFirstName());
            Assert.assertNotEquals("LST", user.getLastName());
            Assert.assertNotEquals("MID", user.getMiddleName());
            USER_SERVICE.updateUser(user.getId(), "FST", "LST", "MID");
            Assert.assertEquals("FST", USER_SERVICE.findById(user.getId()).getFirstName());
            Assert.assertEquals("LST", USER_SERVICE.findById(user.getId()).getLastName());
            Assert.assertEquals("MID", USER_SERVICE.findById(user.getId()).getMiddleName());
        }
    }

    @Test
    public void testSetPasswordNegative() {
        Assert.assertThrows(IdEmptyException.class, () -> USER_SERVICE.setPassword(NULLABLE_USER_ID, NULLABLE_PASSWORD));
        Assert.assertThrows(IdEmptyException.class, () -> USER_SERVICE.setPassword(EMPTY_USER_ID, NULLABLE_PASSWORD));
        Assert.assertThrows(PasswordEmptyException.class, () -> USER_SERVICE.setPassword(UUID.randomUUID().toString(), NULLABLE_PASSWORD));
        Assert.assertThrows(PasswordEmptyException.class, () -> USER_SERVICE.setPassword(UUID.randomUUID().toString(), EMPTY_PASSWORD));
        Assert.assertThrows(UserNotFoundException.class, () -> USER_SERVICE.setPassword(UUID.randomUUID().toString(), UUID.randomUUID().toString()));
    }

    @Test
    public void testSetPasswordPositive() {
        for (final UserDTO user : userList) {
            USER_SERVICE.setPassword(user.getId(), "PSW");
            Assert.assertEquals(HashUtil.salt(PROPERTY_SERVICE, "PSW"), USER_SERVICE.findById(user.getId()).getPasswordHash());
        }
    }

    @Test
    public void testIsLoginExists() {
        Assert.assertFalse(USER_SERVICE.isLoginExist(NULLABLE_LOGIN));
        Assert.assertFalse(USER_SERVICE.isLoginExist(EMPTY_LOGIN));
        Assert.assertTrue(USER_SERVICE.isLoginExist(userList.get(0).getLogin()));
    }

    @Test
    public void testIsEmailExists() {
        Assert.assertFalse(USER_SERVICE.isEmailExist(NULLABLE_EMAIL));
        Assert.assertFalse(USER_SERVICE.isEmailExist(EMPTY_EMAIL));
        Assert.assertTrue(USER_SERVICE.isEmailExist(userList.get(0).getEmail()));
    }

    @Test
    public void testRemoveByLoginNegative() {
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.removeByLogin(NULLABLE_LOGIN));
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.removeByLogin(EMPTY_LOGIN));
    }

    @Test
    public void testRemoveByLoginPositive() {
        for (final UserDTO user : userList) {
            USER_SERVICE.removeByLogin(user.getLogin());
            Assert.assertFalse(USER_SERVICE.findAll().contains(user));
        }
        Assert.assertEquals(0, USER_SERVICE.findAll().size());
    }

    @Test
    public void testRemoveByEmailNegative() {
        Assert.assertThrows(EmailEmptyException.class, () -> USER_SERVICE.removeByEmail(NULLABLE_EMAIL));
        Assert.assertThrows(EmailEmptyException.class, () -> USER_SERVICE.removeByEmail(EMPTY_EMAIL));
    }

    @Test
    public void testRemoveByEmailPositive() {
        for (final UserDTO user : userList) {
            USER_SERVICE.removeByEmail(user.getEmail());
            Assert.assertFalse(USER_SERVICE.findAll().contains(user));
        }
        Assert.assertEquals(0, USER_SERVICE.findAll().size());
    }

    @Test
    public void testFindByEmailNegative() {
        Assert.assertThrows(EmailEmptyException.class, () -> USER_SERVICE.findByEmail(NULLABLE_EMAIL));
        Assert.assertThrows(EmailEmptyException.class, () -> USER_SERVICE.findByEmail(EMPTY_EMAIL));
    }

    @Test
    public void testFindByEmailPositive() {
        for (final UserDTO user : userList) {
            Assert.assertEquals(user.getId(), USER_SERVICE.findByEmail(user.getEmail()).getId());
        }
    }

    @Test
    public void testFindByLoginNegative() {
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.findByLogin(NULLABLE_LOGIN));
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.findByLogin(EMPTY_LOGIN));
    }

    @Test
    public void testFindByLoginPositive() {
        for (final UserDTO user : userList) {
            Assert.assertEquals(user.getId(), USER_SERVICE.findByLogin(user.getLogin()).getId());
        }
    }

    @Test
    public void testFindByIdNegative() {
        Assert.assertThrows(IdEmptyException.class, () -> USER_SERVICE.findById(NULLABLE_USER_ID));
        Assert.assertThrows(IdEmptyException.class, () -> USER_SERVICE.findById(EMPTY_USER_ID));
    }

    @Test
    public void testFindByIdPositive() {
        for (final UserDTO user : userList) {
            Assert.assertEquals(user.getId(), USER_SERVICE.findById(user.getId()).getId());
        }
    }

    @Test
    public void testLockUserByLoginNegative() {
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.lockUserByLogin(NULLABLE_LOGIN));
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.lockUserByLogin(EMPTY_LOGIN));
        Assert.assertThrows(UserNotFoundException.class, () -> USER_SERVICE.lockUserByLogin(UUID.randomUUID().toString()));
    }

    @Test
    public void testLockUserByLoginPositive() {
        for (final UserDTO user : userList) {
            Assert.assertFalse(USER_SERVICE.findByLogin(user.getLogin()).getLocked());
            USER_SERVICE.lockUserByLogin(user.getLogin());
            Assert.assertTrue(USER_SERVICE.findByLogin(user.getLogin()).getLocked());
        }
    }

    @Test
    public void testUnlockUserByLoginNegative() {
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.unlockUserByLogin(NULLABLE_LOGIN));
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.unlockUserByLogin(EMPTY_LOGIN));
        Assert.assertThrows(UserNotFoundException.class, () -> USER_SERVICE.unlockUserByLogin(UUID.randomUUID().toString()));
    }

    @Test
    public void testUnlockUserByLoginPositive() {
        testLockUserByLoginPositive();
        for (final UserDTO user : userList) {
            Assert.assertTrue(USER_SERVICE.findByLogin(user.getLogin()).getLocked());
            USER_SERVICE.unlockUserByLogin(user.getLogin());
            Assert.assertFalse(USER_SERVICE.findByLogin(user.getLogin()).getLocked());
        }
    }

}
