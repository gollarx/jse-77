package ru.t1.shipilov.tm.migration;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.shipilov.tm.marker.UnitCategory;

@Category(UnitCategory.class)

public class TaskSchemeTest extends AbstractSchemeTest {

    @Test
    public void test() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("task");
    }

}
