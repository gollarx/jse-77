package ru.t1.shipilov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.shipilov.tm.api.component.ISaltProvider;

public interface IPropertyService extends ISaltProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getApplicationLog();

    @NotNull
    String getApplicationName();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

    @NotNull
    Integer getServerPort();

    @NotNull
    String getServerHost();

    @NotNull
    String getSessionKey();

    @NotNull
    Integer getSessionTimeout();

    @NotNull
    String getDBUrl();

    @NotNull
    String getDBPassword();

    @NotNull
    String getDBUser();

    @NotNull
    String getDBSchema();

    @NotNull
    String getDBDriver();

    @NotNull
    String getDBL2Cache();

    @NotNull
    String getDBDialect();

    @NotNull
    String getDBShowSQL();

    @NotNull
    String getDBHbm2DDL();

    @NotNull
    String getDBCacheRegion();

    @NotNull
    String getDBQueryCache();

    @NotNull
    String getDBMinimalPuts();

    @NotNull
    String getDBCacheRegionPrefix();

    @NotNull
    String getDBCacheProvider();

}
