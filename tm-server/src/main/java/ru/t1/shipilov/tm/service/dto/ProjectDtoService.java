package ru.t1.shipilov.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.shipilov.tm.repository.dto.IProjectDtoRepository;
import ru.t1.shipilov.tm.api.service.dto.IProjectDtoService;
import ru.t1.shipilov.tm.enumerated.EntitySort;
import ru.t1.shipilov.tm.enumerated.Status;
import ru.t1.shipilov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.shipilov.tm.exception.entity.field.*;
import ru.t1.shipilov.tm.dto.model.ProjectDTO;

import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class ProjectDtoService implements IProjectDtoService {

    @NotNull
    @Autowired
    private IProjectDtoRepository repository;

    @Override
    @SneakyThrows
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.deleteAllByUserId(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<ProjectDTO> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull List<ProjectDTO> result;
        result = repository.findAllByUserId(userId);
        return result;
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        boolean result;
        result = repository.existsByUserIdAndId(userId, id);
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable ProjectDTO result;
        result = repository.getOneByUserIdAndId(userId, id);
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 1) throw new IndexIncorrectException();
        @Nullable ProjectDTO result;
        @Nullable final List<ProjectDTO> projectList;
        final Pageable pageable = PageRequest.of(index - 1, 1);
        projectList = repository.getOneByIndexAndUserId(userId, pageable);
        if (projectList == null || projectList.isEmpty()) throw new ProjectNotFoundException();
        result = projectList.get(0);
        return result;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        repository.deleteByUserIdAndId(userId, id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 1) throw new IndexIncorrectException();
        @Nullable ProjectDTO project;
        @Nullable final List<ProjectDTO> projectList;
        final Pageable pageable = PageRequest.of(index - 1, 1);
        projectList = repository.getOneByIndexAndUserId(userId, pageable);
        if (projectList == null || projectList.isEmpty()) throw new ProjectNotFoundException();
        project = projectList.get(0);
        repository.deleteByUserIdAndId(userId, project.getId());
    }

    @NotNull
    @Override
    @SneakyThrows
    @SuppressWarnings({"unchecked"})
    public List<ProjectDTO> findAll(@Nullable final String userId, @Nullable final EntitySort entitySort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull List<ProjectDTO> result;
        final Sort sortable = Sort.by(Sort.Direction.ASC, entitySort == null ? EntitySort.BY_CREATED.getSortField() : entitySort.getSortField());
        result = repository.findAllSortByUserId(userId, sortable);
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO changeProjectStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final ProjectDTO project;
        project = repository.getOneByUserIdAndId(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        if (status == null) throw new StatusIncorrectException();
        project.setStatus(status);
        repository.save(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO changeProjectStatusByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 1) throw new IndexIncorrectException();
        @Nullable final ProjectDTO project;
        @Nullable final List<ProjectDTO> projectList;
        final Pageable pageable = PageRequest.of(index - 1, 1);
        projectList = repository.getOneByIndexAndUserId(userId, pageable);
        if (projectList == null || projectList.isEmpty()) throw new ProjectNotFoundException();
        project = projectList.get(0);
        if (status == null) throw new StatusIncorrectException();
        project.setStatus(status);
        repository.save(project);
        return project;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName(name);
        project.setUserId(userId);
        if (description != null && !description.isEmpty())
            project.setDescription(description);
        repository.save(project);

    }

    @Override
    @SneakyThrows
    @Transactional
    public void add(@Nullable final String userId, @Nullable final ProjectDTO project) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (project == null) throw new ProjectNotFoundException();
        project.setUserId(userId);
        repository.save(project);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final ProjectDTO project;
        project = repository.getOneByUserIdAndId(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        if (description != null && !description.isEmpty())
            project.setDescription(description);
        repository.save(project);
    }

    @Override
    public void updateByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 1) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final ProjectDTO project;
        @Nullable final List<ProjectDTO> projectList;
        final Pageable pageable = PageRequest.of(index - 1, 1);
        projectList = repository.getOneByIndexAndUserId(userId, pageable);
        if (projectList == null || projectList.isEmpty()) throw new ProjectNotFoundException();
        project = projectList.get(0);
        project.setName(name);
        if (description != null && !description.isEmpty())
            project.setDescription(description);
        repository.save(project);
    }

}
